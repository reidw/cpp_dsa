#include <iostream>
#include <vector>

int factorial(int n)
{
    if (n == 0 || n == 1)
        return 1;
    return n * factorial(n - 1);
}

int fibonacci(int n)
{
    if (n < 2)
        return n;
    return fibonacci(n - 1) + fibonacci(n - 2);
}

void factorial_prompt()
{
    int n {0};

    while (std::cin) {
        std::cout << "> ";
        std::cin >> n; 
        std::cout << factorial(n) << '\n';
    }
}

void fibonacci_prompt()
{
    int n {0};

    while (std::cin) {
        std::cout << "> ";
        std::cin >> n; 
        std::cout << fibonacci(n) << '\n';
    }
}

int main(void)
{
    fibonacci_prompt();

    return 0;
}
