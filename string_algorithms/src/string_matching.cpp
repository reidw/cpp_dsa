#include <iostream>
#include <string>

/*
C++ Substring Finding 

std::string::find

s.find(p) where s is a string and p is a substring to match

returns the index of where the substring appears in the string if found, otherwise, an undefined value which is equal to std::string::npos
*/

void library_match()
{
    std::string t = {"My name is liam.\n"};
    std::string p = {"liam"};
    int i = t.find(p);

    if (i == std::string::npos)
        std::cout << "Substring " << p << " not found in " << t << '\n';
    else
        std::cout << "Substring " << p << " found in:\n" << t <<  "at index " << i << '\n';
}

/*
O(n^2) algorithm that works for all string cases
A boolean variable is set to true with each iteration of the outer loop.
Another loop iterates from 0 to the size of the pattern. If a mismatch is found,
the boolean variable is set to false and the inner loop is broke out of.
At the end of the inner loop, the function returns if the boolean variable 
has not been set to false beforehand. 
*/

int naive_string_matcher(std::string t, std::string p)
{
    unsigned n = t.size();
    unsigned m = p.size();
    bool found = true;

    for (unsigned i = 0; i <= n - m; ++i) {
        found = true;
        for (unsigned j = 0; j < m; ++j) {
            if (t[i + j] != p[j]) {
                found = false;
                break;
            }
        }
        if (found)
            return i;
    }
    return -1;
}

int naive_string_matcher_nonrepeating(std::string t, std::string p)
{
    unsigned n = t.size();    
    unsigned m = p.size();    
    unsigned i = 0;

    while(i <= n - m) {
        unsigned j = 0;

        for (j = 0; j < m; ++j)
            if (t[i + j] != p[j])
                break;
        if (j == m)
            i += m;
        else if (j == 0)
            i += 1;
        else
            i += j;
    }
}
        
int main(void)
{
    std::string t = {"abcadefa"};
    std::string p = {"cad"};

    int match = naive_string_matcher(t, p);

    /*
    while (std::cin)
    {
        std::cout << t << '\n';
        std::cout << "Enter a pattern to find:\n";
        std::cin >> p;

        int i = naive_string_matcher(t, p);

        if (i != -1)
            std::cout << "Match found at: " << i << '\n';
        else
            std::cout << "No matches found.\n";
    }
    */

    return 0;
}