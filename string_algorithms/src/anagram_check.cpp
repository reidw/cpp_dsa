#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>

bool check_if_anagram(std::string s1, std::string s2)
{
    /* 
    DETERMINING WHETHER TWO STRINGS ARE ANAGRAMS WITHOUT SORTING

    Create int array of size 26, each element initialised to 0.

    Iterate over first string, incrementing the int array at the index
    corresponding to the typecasted char value of each string index.

    Iterate over the second string, subtracting from the int array indexes
    corresponding to the typecasted char value of each string index.

    Finally, iterate over the int array. If any element is not equal 
    to 0, the two strings are not anagrams, so return false. After
    the final loop, return true; if the function reaches this point,
    the two strings passed in are anagrams.
    */

    const int n = 26;
    std::vector<int> a(n, 0);

    for (char c : s1)
        ++a[int(c)-97];

    for (char c : s2)
        --a[int(c)-97];
    
    for (int i : a)
        if (i != 0)
            return false;
    
    return true;
}

int main(void)
{
    std::string a; std::cin >> a;
    std::string b; std::cin >> b;

    bool is_anagram = check_if_anagram(a, b);

    if (is_anagram)
        std::cout << a << " and " << b << " are anagrams.\n";
    else
        std::cout << a << " and " << b << " are not anagrams.\n";

    return 0;
}
