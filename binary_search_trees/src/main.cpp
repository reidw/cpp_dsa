#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <binary_tree.h>


BST<int> gen_tree(const std::vector<int>& a);

void show_walks(BST<int> tree);

void show_minmax(BST<int> tree);

void search(BST<int> tree, int k);

void show_insert_order(const std::vector<int> &a);

std::vector<int> rand_vi(int n, int upper);

void show_search(BST<int> tree);


BST<int> gen_tree(const std::vector<int>& a)
{
    BST<int> binary_tree;

    for (int x : a)
        binary_tree.insert(x);

    return binary_tree;
}

void show_walks(BST<int> tree)
{
    std::cout << "Inorder tree traversal:\n";
    tree.inorder_tree_walk();
    std::cout << "\n\n";

    std::cout << "Preorder tree traversal:\n";
    tree.preorder_tree_walk();
    std::cout << "\n\n";

    std::cout << "Postorder tree traversal:\n";
    tree.postorder_tree_walk();
    std::cout << "\n\n";
}

void show_minmax(BST<int> tree)
{
    std::cout << "Tree minimum: " << tree.minimum_iter() << '\n';
    std::cout << "Tree maximum: " << tree.maximum_recursive() << "\n\n";
}

void search(BST<int> tree, int k)
{
    int r = tree.search(k);

    if (r != -1)
        std::cout << k << " is in the tree.\n";
}



void show_insert_order(const std::vector<int> &a)
{
    std::cout << "Insertion Order:\n";
    for (auto it = a.begin(); it != a.end()-1; ++it)
        std::cout << *it << " -> ";
    std::cout << a[a.size()-1] << "\n\n";
}

std::vector<int> rand_vi(int n, int upper)
{
    std::vector<int> a(n);

    for (auto &&x : a) {
        unsigned r = rand() % upper;
        x = r;
    }

    return a;
}

void show_search(BST<int> tree)
{
    const int n_searches = 10;
    const int node_range = 100;

    std::vector<int> a = rand_vi(n_searches, node_range);

    for (auto &&x : a)
        search(tree, x);
}

int main(void)
{
    srand(time(nullptr));

    const int num_nodes = 10;
    const int node_range = 100;

    std::vector<int> a = rand_vi(num_nodes, node_range);

    show_insert_order(a);

    BST<int> tree = gen_tree(a);
    
    show_walks(tree);
    show_minmax(tree);
    show_search(tree);

    return 0;
}
