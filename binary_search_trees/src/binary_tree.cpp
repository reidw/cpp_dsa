#include <binary_tree.h>

template class BSTNode<int>;
template class BST<int>;

template <class T>
void BST<T>::insert(const T& o)
{
    BSTNode<T> *p = root, *prev = nullptr;

    while (p != nullptr) {
        prev = p;
        if (o < p->data)
            p = p->left;
        else
            p = p->right;
    }

    if (root == nullptr)
        root = new BSTNode<T>(o);
    else if (o < prev->data)
        prev->left = new BSTNode<T>(o);
    else
        prev->right = new BSTNode<T>(o);
}

template <class T>
void BST<T>::inorder_tree_walk(BSTNode<T> *p)
{
    if (p != nullptr) {
        inorder_tree_walk(p->left);
        visit(p);
        inorder_tree_walk(p->right);
    }
}

template <class T>
void BST<T>::preorder_tree_walk(BSTNode<T> *p)
{
    if (p != nullptr) {
        visit(p);
        preorder_tree_walk(p->left);
        preorder_tree_walk(p->right);
    }
}

template <class T>
void BST<T>::postorder_tree_walk(BSTNode<T> *p)
{
    if (p != nullptr) {
        postorder_tree_walk(p->left);
        postorder_tree_walk(p->right);
        visit(p);
    }
}

template <class T>
void BST<T>::inorder_tree_walk()
{
    inorder_tree_walk(root);
}

template <class T>
void BST<T>::preorder_tree_walk()
{
    preorder_tree_walk(root);
}

template <class T>
void BST<T>::postorder_tree_walk()
{
    postorder_tree_walk(root);
}

template <class T>
T* BST<T>::search(BSTNode<T>* x, const T& k) const
{
    while (x != nullptr) 
        if (k == x->data)
            return &x->data;
        else if (k < x->data) 
            x = x->left;
        else
            x = x->right;
    return nullptr;
}

template<class T>
T BST<T>::search(int k)
{
    int* p = search(root, k);
    if (p != nullptr)
        return *p;
    else
        std::cout << k << " not found.\n";
    return -1;
}

template<class T>
T BST<T>::maximum_iter(BSTNode<T>* p)
{
    while (p->right != nullptr)
        p = p->right;
    return p->data;
}

template<class T>
T BST<T>::minimum_iter(BSTNode<T>* p)
{
    while (p->left != nullptr)
        p = p->left;
    return p->data;
}

template<class T>
BSTNode<T>* BST<T>::maximum_recursive(BSTNode<T>* p)
{
    if (p->right != nullptr)
        return maximum_recursive(p->right);
    else
        return p;
}

template<class T>
BSTNode<T>* BST<T>::minimum_recursive(BSTNode<T>* p)
{
    if (p->left != nullptr)
        return minimum_recursive(p->left);
    else
        return p;
}

template<class T>
T BST<T>::maximum_recursive()
{
    BSTNode<T>* p = maximum_recursive(root);
    return p->data;
}

template<class T>
T BST<T>::minimum_recursive()
{
    BSTNode<T>* p = minimum_recursive(root);
    return p->data;
}

template<class T>
T BST<T>::maximum_iter()
{
    return maximum_iter(root);
}

template<class T>
T BST<T>::minimum_iter()
{
    return minimum_iter(root);
}
