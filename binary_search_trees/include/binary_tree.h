#ifndef BINARY_TREE_H
#define BINARY_TREE_H

#include <iostream>

template <class T>
class BSTNode {
    public:
    BSTNode() { left = right = nullptr; }
    BSTNode(const T& data):data(data) {}
    T data;
    BSTNode<T> *left, *right;
};

template <class T>
class BST {
    public:
    BST() { root = nullptr; }
    void insert(const T& o);
    void inorder_tree_walk();
    void preorder_tree_walk();
    void postorder_tree_walk();
    T maximum_iter();
    T minimum_iter();
    T maximum_recursive();
    T minimum_recursive();
    T search(int k);

    private:
    virtual void visit(BSTNode<T>* p)
    {
        std::cout << p->data << ' ';
    }
    void inorder_tree_walk(BSTNode<T> *p);
    void preorder_tree_walk(BSTNode<T> *p);
    void postorder_tree_walk(BSTNode<T> *p);
    T maximum_iter(BSTNode<T>* p);
    T minimum_iter(BSTNode<T>* p);
    BSTNode<T>* maximum_recursive(BSTNode<T>* p);
    BSTNode<T>* minimum_recursive(BSTNode<T>* p);
    T* search(BSTNode<T>* x, const T& k) const;
    BSTNode<T>* root;
};

#endif
