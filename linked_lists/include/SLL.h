#ifndef SLL_H
#define SLL_H

template<class T>
class SLLNode {
    public:
    SLLNode() { next = nullptr; }
    SLLNode(const T& data, SLLNode *next = nullptr)
        :data(data), next(next)
    {}
    T data;
    SLLNode *next;
};

template<class T>
class SLL {
    public:
    SLL() { head = tail = nullptr; }
    ~SLL();
    T is_empty() { return head == nullptr; }
    void add_to_head(T data);
    void add_to_tail(T data);
    T delete_from_head();
    T delete_from_tail();
    void delete_node(T data);
    bool is_in_list(T data) const;
    bool has_cycle();

    private:
    SLLNode<T> *head, *tail;
};

#endif