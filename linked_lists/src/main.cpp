#include <SLL.h>
#include <iostream>

void node_test(void)
{
    // manually create three singly-linked nodes and print their values
    SLLNode<int> *p = new SLLNode<int>(10);
    p->next = new SLLNode<int>(8);
    p->next->next = new SLLNode<int>(5);
}

void SLL_test(void)
{
    // define a singly-linked list of ints
    SLL<int> sll_ints;

    // add node at beginning of list
    sll_ints.add_to_head(1);
    sll_ints.add_to_head(2);
    sll_ints.add_to_head(3);
    sll_ints.add_to_head(4);
}

int main(void)
{
    node_test();

    return 0;
}