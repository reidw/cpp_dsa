#include <SLL.h>

template class SLL<int>;
template class SLL<double>;

template <class T>
SLL<T>::~SLL()
{
    for (SLLNode<T> *p; !is_empty(); ) {
        p = head->next;
        delete head;
        head = p;
    }
}

template <class T>
void SLL<T>::add_to_head(T data)
{
    head = new SLLNode<T>(data, head);
    if (tail == nullptr)
        tail = head;
}

template <class T>
void SLL<T>::add_to_tail(T data)
{
    if (tail != nullptr) {
        tail->next = new SLLNode<T>(data);
        tail = tail->next;
    } else 
        head = tail = new SLLNode<T>(data);
}

template<class T>
T SLL<T>::delete_from_head()
{
    T data = head->data;
    SLLNode<T> *temp = head;
    if (head == tail)
        head = tail = nullptr;
    else
        head = head->next;
    delete temp;
    return data;
}

template<class T>
T SLL<T>::delete_from_tail()
{
    T data = tail->data;
    if (head == tail) {
        delete head;
        head = tail = nullptr;
    } 
    else {
        SLLNode<T> *temp;
        for (temp = head; temp->next != tail; temp = temp-> next);
        delete tail;
        tail = temp;
        tail->next = nullptr;
    }
    return data;
}

template<class T>
void SLL<T>::delete_node(T data)
{
    if (head != nullptr)
        if (head == tail && data == head->data) { // one node in list
            delete head;
            head = tail = nullptr;
        }
        else if (data == head->data) { // more than one node in list
            SLLNode<T> *temp = head;
            head = head->next;
            delete temp;
        }
        else {
            SLLNode<T> *prev, *temp;
            for (prev = head, temp = head->next; temp != nullptr && !(temp->data == data); prev = prev->next, temp = temp->next);
            if (temp != nullptr) {
                prev->next = temp->next;
                if (temp == tail)
                    tail = prev;
                delete temp;
            }
        }
}

template <class T>
bool SLL<T>::is_in_list(T data) const
{
    SLLNode<T>* temp;
    for (temp = head; temp != nullptr && !(temp->data == data); temp = temp->next);
    return temp != nullptr;
}

template <class T>
bool SLL<T>::has_cycle()
{
    if (head == nullptr) return false;
    SLLNode<T> *fast = head->next;
    SLLNode<T> *slow = head;
    while (fast != nullptr && fast->next != nullptr && slow != nullptr) {
        if (fast == slow)
            return true;
        fast = fast->next->next;
        slow = slow->next;
    }
    return false;
}