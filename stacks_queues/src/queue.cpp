#include <queue.h>

template class Queue<int>;
template class Queue<double>;
template class Queue<char>;

template<class T>
bool Queue<T>::is_empty()
{
    return head == nullptr;
}

template<class T>
T Queue<T>::peek()
{
    return head->data;
}

template<class T>
void Queue<T>::add(T o)
{
    QNode<T> *p = new QNode<T>(o);

    if (tail != nullptr)
        tail->next = p;

    tail = p;

    if (head == nullptr)
        head = p;
}

template<class T>
int Queue<T>::remove()
{
    T data = head->data;
    head = head->next;
    if (head == nullptr)
        tail = nullptr;
    return data;
}

template<class T>
void check_if_empty(Queue<T> q)
{
    if (q.is_empty())
        std::cout << "Empty queue.\n";
    else
        std::cout << "Queue not empty.\n";
}