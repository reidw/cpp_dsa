#include <stack.h>

template class Stack<int>;
template class Stack<double>;
template class Stack<char>;

template<class T>
bool Stack<T>::is_empty()
{
    return top == nullptr;
}

template<class T>
T Stack<T>::peek()
{
    return top->data;
}

template <class T>
void Stack<T>::push(T o)
{
    SNode<T> *sn = new SNode<T>(o);
    sn->next = top;
    top = sn;
}

template <class T>
T Stack<T>::pop()
{
    // should probably have check if
    // node is null pointer
    T data = top->data;
    top = top->next;
    return data;
}