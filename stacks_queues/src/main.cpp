#include <stack.h>
#include <queue.h>
#include <stack>

void queue_test(int n)
{
    std::cout << "Queue Test:\n";

    Queue<int> q = Queue<int>();

    for (int i = 0; i < n; ++i)
        q.add(i);

    for (int i = 0; i < n; ++i)
        std::cout << q.remove() << '\n';
}

void stack_test(int n)
{
    std::cout << "Stack Test:\n";

    Stack<int> s = Stack<int>();

    for (int i = 0; i < n; ++i)
        s.push(i);

    for (int i = 0; i < n; ++i)
        std::cout << s.pop() << '\n';
}

int main(void)
{
    /*
    int n {4};

    queue_test(n);

    stack_test(n);
    */

   std::stack<int> stack;

    return 0;
}