#ifndef QUEUE_H
#define QUEUE_H

#include <iostream>
#include <string>
#include <vector>

template<class T>
class QNode {
    public:
    QNode() 
    {
        next = prev = 0;
    }
    QNode(const T& d, QNode *n = 0, QNode *p = 0)
    {
        data = d;
        next = n;
        prev = p;
    }
    T data;
    QNode *next, *prev;
};

template<class T>
class Queue {
    public:
    bool is_empty();
    T peek();
    void add(T o);
    int remove();
    private:
    QNode<T> *head, *tail;
};

#endif