#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <string>
#include <vector>

template<class T>
class SNode {
    public:
    SNode() 
    {
        next = 0;
    }
    SNode(const T& d, SNode *n = 0)
    {
        data = d;
        next = n;
    }
    T data;
    SNode *next;
};

template<class T>
class Stack {
    public:
    bool is_empty();
    T peek();
    void push(T o);
    T pop();
    private:
    SNode<T> *top;
};

#endif