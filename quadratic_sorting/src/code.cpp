#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <limits>

void swap(int& a, int& b)
{
    // version of swap that uses a temp/third variable
    int temp = a;
    a = b;
    b = temp;
}

void swap2(int& a, int& b)
{
    // version of swap that uses arithmetic with two variables
    a = a + b;
    b = a - b;
    a = a - b;
}

void selection_sort(std::vector<int>& a)
{
    int min {0}, k {0};

    for (unsigned i = 0; i < a.size(); ++i) {
        k = i;
        min = a[i];
        for (unsigned j = i; j < a.size(); ++j) {
            if (a[j] < min) {
                min = a[j];
                k = j;
            }
        }
        if (a[i] != a[k])
            swap(a[i], a[k]);
    }
}

void insertion_sort(std::vector<int>& a)
{
    int j {0}, k {0};

    for (unsigned i = 1; i < a.size(); ++i) {
        k = a[i];
        j = i - 1;
        while (j >= 0 && a[j] > k) {
            a[j + 1] = a[j];
            j -= 1;
        }
        a[j + 1] = k;
    }
}

bool valid_order(std::vector<int>& a)
{
    for (unsigned i = 0; i < a.size()-1; ++i) 
        if (a[i+1] < a[i])
            return false;
    return true;
}

int rand_int()
{
    const int min {1}, max {100};  
    return (rand() % max) + min;
}

// generate a random vector of ints of a given length
std::vector<int> random_vi(int length)
{
    int n {1}, count {length+1};
    std::vector<int> vi;

    std::cout << "Generating a random array of " << length << " elements...\n";
    while (--count) {
        vi.push_back(rand_int());
        std::cout << n << ": " << vi[n-1] << '\n';
        ++n;
    }
    return vi;
}

int main(void)
{
    srand(time(NULL));

    std::vector<int> arr {random_vi(12)};

    selection_sort(arr);

    insertion_sort(arr);

    std::cout << "Sorted elements\n";

    for (unsigned i : arr) std::cout << i << '\n';

    if (valid_order(arr))
        std::cout << "Sorting operation was successful.\n";
    else
        std::cout << "Sorted operator was NOT successful.\n";

    return 0;
}
